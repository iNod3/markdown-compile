#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='markdwon-compule',
      version='1.0',
      description='Compile markdown with @include directive',
      author='',
      author_email='',
      url='',
      packages=find_packages(),
      zip_safe=False,
      scripts=[
        'bin/markdown-compile.py',
      ],
     )
import sys

import os
import argparse

def mdcompile(markdown):

    import re, io

    if isinstance(markdown, str):
        markdown = open(markdown, 'r')

    IoCompiled = io.StringIO()
    reInclude = re.compile('@include "(.*)"')
    reComment = re.compile('<!-- .* -->')
    
    for line in markdown.readlines():
        if reInclude.match(line) is None or reComment.match(line) is not None:
            IoCompiled.write(line)
        else:
            IncludeFile = reInclude.findall(line)[0]
            IoCompiled.write('<!-- Content including from %s -->\n' % IncludeFile)
            IoCompiled.write(mdcompile(IncludeFile))
            IoCompiled.write('\n')

    IoCompiled.seek(0, 0)
    return(IoCompiled.read())

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('index', type=argparse.FileType('r'), default=sys.stdin, help='Index file')

    args = parser.parse_args()

    Compiled = '%s-compiled.md' % os.path.splitext(args.index.name)[0]

    open(Compiled, 'w').write(mdcompile(args.index))
